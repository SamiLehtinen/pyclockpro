PyClockPro
==========

PyClockPro is open source CLOCK-Pro caching algorithm implementation
for Python 3.2+.

This is my hobby project for learning more about Python performance details.
I also need good cache for my work related projects. Both ARC and CAR are
unfortunately patented.

Original performance goal was to be faster than `functools.lru_cache` at 50% hit
ratio (LRU based). At this point I can conclude that this goal wasn't met. 
Current implementation is roughly 100% slower than `lru_cache` in `functools`. 
But CLOCK-Pro provides better hit ratio when caching same amount of keys.

CLOCK-Pro should also combine best worlds of CLOCK and LIRS, so it should
out perform CAR and ARC which easily outperform LRU. In terms of hit ratio
and CPU time consumed by the cache algorithm.

See: TODO, README, BENCHMARK and LICENSE (MIT)

Current status
==============

First version is now ready, I would love to get informal peer review done by
someone. All feedback is really welcome.

How does it work?
=================

Pretty much works like mentioned in documentation. Both of these documents
lack multiple minor details required for making actually functioning version.
I recommend reading the source if you're really curious.

CLOCK-Pro documentation:

1. [CLOCK-Pro: An Effective Improvement of the CLOCK Replacement (2005, Usenix)](http://www.ece.eng.wayne.edu/~sjiang/pubs/papers/jiang05_CLOCK-Pro.pdf)
2. [Clock-Pro: An Effective Replacement in OS Kernel](http://www.slideshare.net/huliang64/clockpro)
3. [Wikipedia: LIRS caching algorithm](https://en.wikipedia.org/wiki/LIRS_caching_algorithm)

How to use it?
==============

Here are basic features like retrieving and storing data. Advanced topics
include using default data sources, writeback caching, resizing cache,
clearing cache, flushing writeback data and advanced PyClockPro statistics.

Basic features
--------------

First import PyClockPro library  
`from pyclockpro import cp_cache`

### Using PyClockPro as decorator  
    @cp_cache(128)  
    def myfunc(value):  
        print('Not cached:',value)  
        return value  
        
This is exactly comparable to `lru_cache` decorator from `functools`.

### Using PyClockPro as dictionary  

Create instance with user maximum cache size  
`pcp=pyclockpro.PyClockPro(128)`  
  
Store data to cache  
`pcp[1]='One'`  
Alternate access method:  
`pcp.set(1,'One')`
  
Read data from cache  
`print(pcp[1])`  
Alternate access method:  
`print(pcp.get(1))`

If data isn't in cache and default or data source is not specified, you'll
get `KeyError` exception.

Most important part, when cache is full, CLOCK-Pro eviction algoritm will be
utilized to select data to evict.

Advanced
--------

 * `clear()` clear cache data, keys and statistics. Does not reset handlers or
 cache size.
 * `set_size(int)` define a new cache size as # of pages, runs eviction if 
 required.
 * `cache_info()` read cache (read) hit statistics. Returns `namedtuple` like
 `lru_cache: hits misses maxsize currsize`. 
 * `cache_stats()` extended PyClockPro information returns `namedtuple` with
 `gethits getmisses sethits setmisses memmax memcold size counthot countcold
 counttest`.
 * `is_cached(key)` returns boolean telling if key is being cached. Does not
 update key's reference information.
 * `set_default(key, default)` if key does not exist, set default value for
 it and return it, else return keys current value. Does not raise `KeyError`.
 * `get(key, default)` get value of key or return default instead of raising
 `KeyError`.
 * `set_hander_read(func)` set data source handler function. This function is
 called with key as parameter instead of returning default or raising.
 `KeyError`. Func can raise KeyError which is passed through, or preferrably
 return None which is then cached, preventing further miss lookups.
 * `set_hander_write(func)` set writeback handler function, this function is
 called with key and value when data is being evicted from cache.
 * `flush()` Call write-back data handler once for every page in cache. See
 details from source. Destructor does call this, so basically you shouldn't use
 this at all.

Debug features
--------------

`dump_state(hot_pos=bool, color=bool)` returns internal state dump string
for debugging. If hot_pos is `True` dump starts always from hot hand postion.
If color is `True` HTML font tags will be included for nicer color web
presentation.
  
    Returned string legend: (CLOCK-Pro paper terms)  
    2 hot hand  
    1 cold hand  
    0 test hand  
    H hot referenced page (hot page)  
    h hot non-referenced page  
    C cold referenced page (resident cold page)  
    c cold non-referenced page  
    t test page, key only, no page (non-resident cold page)  
  
Sample:  
`hhhhHhhHHhhhhhhhhhh0nnnnnnnnnnnnnnnnnnhnnnnnnnnnnnn1c2HHHHHHHHh`  
A longer HTML internal state dump version is available
[here](http://www.sami-lehtinen.net/misc/clock-pro-internal-state-dump-sample).

If you're really curious, you can read data directly from `_meta` list and
 `_data` dictionary.

Other files
===========

 * `unittest.py` contains unit tests for PyClockPro project. - Not done yet
 * `benchmark.py` contains benchmark tests for PyClockPro project.  - Source
 is still unpublished.

Benchmarks
==========

There's no point to provide alternate caching algorithm implementation unless
it's better than existing solutions. I think benchmarks are best way to show
if this project is successful or a failure.

Benchmark generates pretty tests chart where it compares LRU and PCP caches.
Tests are run using diffrenet key distributions so that there will be hit rates
from 0% to 100% using 5% steps. Because distribution of data is very important,
sets will be generated using Pareto distribution, Gaussian distribution and
pure classic random. 

Before generated data set is used for further testing, it is checked
using LRU cache so that it actually provides required hit rate. Remember random
data sets are random, no hit ratio is guaranteed based on input values when
generating set.

Output contains hit ratio and wall clock time comparisons, where lru cache is
used as baseline. Charts used Pareto distribution based random data.

Results
-------

![Comparison chart](https://lh5.googleusercontent.com/LchutVB8Ds0cb8DKG5O0py1xCe_SIG8ommYvt-RkAm2WgtHizOxIhcy4XEy2kWwwH5-Jy-BYcnryqV_skfm6a2UgVa0_C8hCUYoVfGIihGMWQSS7MA=w572 "CLOCK-Pro vs LRU hit ratio comparison chart")  

![Comparison chart](https://lh5.googleusercontent.com/t4IfhDgCL78UQr-c5rwD8jqn5g88eyDgXH4dtVAiNpMjy-Ze8f82qdbAcw58RCN9HRw9mnDnbhf7RYiGdMkCnZh3NnCApOJhoSHh3ewN5f11M21_uDbh=w572 "CLOCK-Pro vs LRU CPU Time consumption comparison chart")  

[CLOCK-Pro vs LRU higher resolution charts (PDF)](https://drive.google.com/file/d/0B87lb0tFMCBFc3Uxd1VoMjhUeWM/view)

Detailed benchmark results are in BENCHMARK file.

Conclusions
-----------

Based on this information CLOCK-Pro cache eviction is more expensive at low
hit ratios, but that's the area where it give much better hit ratio than LRU.
Therefore it's easy to conclude that CLOCK-Pro is generally clearly superior
compared to LRU. Especially in situations where CPU power is cheap compared to
producing the value being cached. In these tests, the cache returned same value
as the key. Which means that producing the cached data was virtually free as
CPU time cost.

# Project information
Project GIT repository: [PyClockPro](https://bitbucket.org/SamiLehtinen/pyclockpro)
Project Author: [Sami Lehtinen](http://www.sami-lehtinen.net/)

Keywords:  
LRU, SEQ, EELRU, UBM, FBR, LRFU, MRU, LRU-2, 2Q, MQ, LIRS, ARC, CAR, GCLOCK
SARC, MIN, cache, caching, cached, python 3, library, page, eviction, hit, miss,
efficient, hand, adaptive, performance, demand, paging, cache, replacement,
policy, hot, cold, test, statistics, performance, algorithm, class, marking
mark, Python 3.2, Python 3.3, PCP, pure Python, approximation, memoize, 
memoization, purge, purning, function, method, replace, drop in.
